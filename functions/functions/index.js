const functions = require('firebase-functions');
const express = require('express');

const app = express();

app.get("/", (req, res) => {
    res.json({
        message: "Denaaaaaaaaaxxx..........!!!!!"
    });
});

app.get("/about", (req, res) => {
    res.json({
        message: "Den otiiiiiiiiii..........!!!!!"
    });
});

app.use((req, res) => {
    res.json({
        message: "404 Ma afouuu deeeennnaaaaaaaaaxx !!!!"
    });
});



exports.api = functions.https.onRequest((request, response) => {
    if (!request.path) request.url = `/${request.url}`;
    return app(request, response);
})