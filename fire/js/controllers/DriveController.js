app.controller("DriveController", function ($scope, $rootScope, $firebaseStorage) {

    // $scope.uploadFile = function(){
    //     // Select file
    //     var myFile = document.getElementById('myFile');
    //     var file = myFile.files[0];

    //     // Create reference
    //     var storageRef = firebase.storage().ref("drive/"+$rootScope.user.$id+"/"+file.name);
    //     $scope.drive = $firebaseStorage(storageRef);

    //     $scope.drive.$getMetadata()
    //     .then(function(data) {
    //         alert('yparxei');
    //     })
    //     .catch(function(){
    //         // Upload file
    //         var uploadTask = $scope.drive.$put(file);
    //         uploadTask.$progress(function(snapshot) {
    //             var percentUploaded = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    //             console.log(percentUploaded);
    //         });
    //     });
    // };

    var storageRef = firebase.storage().ref("drive/"+$rootScope.user.$id+"/");

    $scope.uploadProgress = 0;
    $scope.uploadFile = function(){
        // Select file
        var myFile = document.getElementById('myFile');
        var file = myFile.files[0];

        $scope.fileRef = $firebaseStorage(storageRef.child(file.name));
        var uploadTask = $scope.fileRef.$put(file);

        uploadTask.$progress(function(snapshot) {
            $scope.uploadProgress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        });

        uploadTask.$complete(function(){
            $scope.uploadProgress = 0;
        });
    };
});