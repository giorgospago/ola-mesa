app.controller("MainController", function ($scope) {
    $scope.list = [
        {id: 28, name: "Stelios Karydakis", age: 24, email: "xxx@porkhub.com"},
        {id: 63, name: "Nikos Karydakis", age: 13, email: "xxx2@porkhub.com"},
        {id: 85, name: "Softex Karydakis", age: 5, email: "xxx3@porkhub.com"},
        {id: 22, name: "Sokraths Karydakis", age: 88, email: "xxx4@porkhub.com"},
        {id: 54, name: "Dimos Karydakis", age: 40, email: "xxx5@porkhub.com"}
    ];

    $scope.min = 0;
    $scope.max = 100;
});

app.filter("adult", function(){
    return function(items, min, max){
        return items.filter(x => x.age >= min && x.age <= max);
    };
});

app.filter("softex", function(){
    return function(items){
        return items.map(x => {
            x.name = x.name.toUpperCase();
            return x;
        });
    };
});

app.directive("nikos", function() {
    return {
        restrict: "AE",
        template : "<h1>Nikos o eksypnos !!!</h1>"
    };
});

app.directive("person", function() {
    return {
        restrict: "E",
        scope: {
            p: "=personData",
            c: "=color",
        },
        templateUrl : "views/person.html"
    };
});