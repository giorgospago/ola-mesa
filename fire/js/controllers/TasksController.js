app.controller("TasksController", function ($scope, $rootScope, $firebaseObject) {
    $scope.tasks = [];
    $firebaseObject(db.ref('tasks/'+$scope.user.$id)).$bindTo($scope, "tasks");

    $scope.task = {
        title: ""
    };

    $scope.taskErr = null;
    $scope.saveTask = function(){
        $scope.taskErr = null;
        
        db.ref('tasks/'+$scope.user.$id).push({
            title: $scope.task.title
        })
        .catch(function(err){
            $scope.taskErr = err;
        });

        $scope.task = {
            title: ""
        };
    };


    $scope.removeTask = function(taskId){
        db.ref('tasks/'+$scope.user.$id+"/"+taskId).remove();
    };
});